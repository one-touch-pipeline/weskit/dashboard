# WESkit Dashboard

A Webinterface for the GA4GH compliant Workflow-Execution-Service WESkit.

You find more documentation in the [Wiki](https://gitlab.com/one-touch-pipeline/weskit/documentation).
