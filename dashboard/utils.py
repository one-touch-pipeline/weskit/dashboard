import pandas as pd


def to_df(json_data):
    data = pd.json_normalize(json_data)
    data = data.rename(index={0: "Value"})
    data.index.name = "Key"
    return data.transpose()


def state_counts(data):
    data = data.get("system_state_counts")
    states = list(data.keys())
    counts = list(data.values())
    counts_sum = sum(counts)

    if counts_sum > 0:
        freq = [round((counts[i] / counts_sum * 100), 2) for i in range(len(counts))]
    else:
        freq = [0 for i in range(len(counts))]

    states_dict = [{"state": state, "absolute": count, "frequency": freq}
                   for state, count, freq in zip(states, counts, freq)]
    return states, counts, states_dict
