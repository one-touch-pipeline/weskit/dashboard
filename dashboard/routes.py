from functools import wraps
from flask import (render_template,
                   current_app,
                   request,
                   Blueprint,
                   Response,
                   jsonify,
                   redirect,
                   session)
from dashboard.com import Com
from dashboard.forms import CreateRun
import dashboard.utils as u
from flask_jwt_extended import current_user
from flask_jwt_extended import verify_jwt_in_request, unset_jwt_cookies
import logging
import urllib
from typing import Tuple, Union
from dashboard.oidc.utils import requester_and_cookieSetter
from dashboard.workflows import get_workflow_data

bp = Blueprint('main', __name__)

# creating instance of the communication class
commu = Com()
logger = logging.getLogger(__name__)


def validate_user_cookie():
    def wrapper(fn):
        @wraps(fn)
        def decorator(*args, **kwargs):
            if current_app.config["LOGIN"]:
                if get_username() == "dummy":
                    return login()
            return fn(*args, **kwargs)
        return decorator
    return wrapper


def validate_login(optional: bool = False):
    def wrapper(fn):
        @wraps(fn)
        def decorator(*args, **kwargs):
            try:
                cookie_header = request.headers.get('Cookie')
                csrf_token = None
                if cookie_header:
                    try:
                        cookies = dict(item.strip().split('=', 1)
                                       for item in cookie_header.split(';')
                                       if '=' in item)
                        csrf_token = cookies.get('X-CSRF Token')
                        if csrf_token:
                            request.environ['HTTP_X_CSRF_TOKEN'] = csrf_token
                    except ValueError as e:
                        raise e
                csrf_token_cookie = request.cookies.get('X-CSRF Token')
                csrf_token_header = request.headers.get('X-CSRF-Token')

                # Validate CSRF Token by comparing cookie and header values
                if csrf_token_cookie != csrf_token_header:
                    logger.error("CSRF token mismatch!")
                    return jsonify({"msg": "CSRF token mismatch!"}), 403

                verify_jwt_in_request(optional=(optional or not current_app.config["LOGIN"]))
            except Exception as e:
                logger.error(f"JWT validation failed: {e}")
                return render_template(
                    "login_required.html",
                    title="Login required",
                    user=get_username(),
                    login=current_app.config["LOGIN"]), 401
            return fn(*args, **kwargs)
        return decorator
    return wrapper


def get_username():
    if current_app.config["LOGIN"]:
        if current_user:
            return current_user.preferred_username
    return False


@bp.route("/")
@bp.route("/dashboard")
@validate_login(optional=True)
@validate_user_cookie()
def Contact():
    logger.info("Call root endpoint")
    info = current_app.config["INFO"]
    return render_template(
        "information.html",
        title="Information",
        info=info,
        user=get_username(),
        login=current_app.config["LOGIN"])


@bp.route("/token")
@validate_login(optional=True)
@validate_user_cookie()
def token():
    token = request.cookies.get('access_token_cookie')
    return render_template(
        "token.html",
        token=token,
        user=get_username(),
        login=current_app.config["LOGIN"])


@bp.route("/runs/<RunID>")
@validate_login()
@validate_user_cookie()
def run(RunID):
    this_run_json = commu.GetRun(RunID)
    this_run = u.to_df(this_run_json)
    state = this_run_json["state"]
    return render_template(
        "run.html",
        title="Run",
        run_id=RunID,
        run=[this_run.to_html(classes="run")],
        user=get_username(),
        login=current_app.config["LOGIN"],
        state=state)


@bp.route("/runs")
@validate_login()
@validate_user_cookie()
def runs():
    runs_list = commu.getRunsExtended()
    return render_template(
        "runs.html",
        title="Runs",
        user=get_username(),
        login=current_app.config["LOGIN"],
        runs=runs_list)


@bp.route("/submit", methods=['GET', 'POST'])
@validate_login()
@validate_user_cookie()
def create_run() -> str:
    form = CreateRun()
    if form.validate_on_submit():
        config_text = request.form.get("config")
        workflow_data = get_workflow_data(request.form.get("workflow"), config_text)
        post_run = commu.postRun(workflow_data=workflow_data)
        if "run_id" not in post_run:
            return post_run["content"]
        else:
            run_id = post_run["run_id"]
        return render_template("submit.html", run_id=run_id)
    return render_template(
        "create_run.html",
        user=get_username(),
        login=current_app.config["LOGIN"],
        form=form)


@bp.route('/login', methods=['GET'])
def login(requestedURL: str = "") -> Response:
    """
    Browser Login via Redirect to Keyclaok Form
    """
    url = current_app.OIDC_Login.oidc_config['authorization_endpoint']
    params = (
                 "?client_id=%s&"
                 "redirect_uri=%s&"
                 "scope=%s&"
                 "access_type=offline&"
                 "response_type=code&"
                 "openid.realm=%s&"
                 "state=%s") % (
                 current_app.config["OIDC_CLIENTID"],
                 urllib.parse.quote(
                     current_app.config["DASHBOARD_URL"] + '/login/callback', safe=''
                 ),
                 "+".join(current_app.config['OIDC_SCOPE']),
                 current_app.config['OIDC_REALM'],
                 urllib.parse.quote(requestedURL)
    )

    return redirect(url + params, code=302)


@bp.route('/login/callback', methods=['GET'])
def login_callback() -> Union[Response, Tuple[Response, int]]:
    """
    The ODIC authenticator redirects to this endpoint after login success and sets an one
    time session code as param. This code can be used to obtain the access_token from the
    OIDC Identity provider. If this function is called by the client it will recieve a session
    cookie and an access token via cookie. Furthermore, the client will be redirected to the
    original requested endpoint.
    """

    code = request.args.get("code", None)
    # Payload
    payload = {
        "grant_type": "authorization_code",
        "code": code,
        "redirect_uri": "%s/login/callback" %
                        (current_app.config["DASHBOARD_URL"]),
        "client_id": current_app.config["OIDC_CLIENTID"],
        "client_secret": current_app.config['OIDC_CLIENT_SECRET']
    }

    if not code:
        return (
            jsonify(
                {'login': False, 'msg': 'login code missing'}
            ), 401
        )
    response_object = None

    response_object = redirect(
        request.url_root.rstrip("/") + urllib.parse.unquote("/dashboard"),
        code=302)

    # Make request
    return (requester_and_cookieSetter(payload, response_object))


@bp.route('/login/logout', methods=['GET'])
def logout() -> Response:
    """
    This deletes the token cookies and removes the SSO session generated by
    the OIDC provider.
    """
    # ODIC host will return to this point after logout
    comebackURL = current_app.config["DASHBOARD_URL"] + "/"

    # Get the end session endpoint URL from the OIDC configuration
    url = current_app.OIDC_Login.oidc_config['end_session_endpoint']

    id_token = session.get('id_token', None)

    # Prepare the new logout parameters
    params = {
        'post_logout_redirect_uri': comebackURL
    }

    if id_token:
        params['id_token_hint'] = id_token
    else:
        # Use the client_id if the id_token_hint is missing
        client_id = current_app.config["OIDC_CLIENTID"]
        params['client_id'] = client_id

    # Construct the full logout URL
    logout_url = url + '?' + urllib.parse.urlencode(params)

    # Initiate redirect to the OIDC provider logout
    oidc_logout = redirect(logout_url, code=302)

    # Delete Token Cookies
    unset_jwt_cookies(oidc_logout)
    oidc_logout.delete_cookie('name')
    oidc_logout.delete_cookie('X-CSRF Token')

    return oidc_logout
