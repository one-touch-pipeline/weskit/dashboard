import enum


class ErrorCodes(enum.Enum):
    SUCCESS = 0
    CONFIGURATION_ERROR = 1
    LOGIN_CONFIGURATION_ERROR = 2
