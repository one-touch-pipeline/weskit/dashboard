import requests
from flask import current_app, jsonify
from flask_jwt_extended.utils import set_access_cookies
from dashboard.oidc.ErrorCodes import ErrorCodes
from typing import Generic, Optional, Any, Dict
import logging
import uuid

logger = logging.getLogger(__name__)


def validate_received_config_or_stop(config: Any) -> Optional[dict]:

    """
    This function validates that the config received from the Identity Provider
    has the correct format and contains all required data.
    :param config:
    :return:
    """

    if not isinstance(config, dict):
        logger.exception(
            "The configuration received from Identity Provider doesn't have the expected type dict!"
            "The type is %s. The server will be stopped now!" % str(type(config))
        )
        exit(ErrorCodes.LOGIN_CONFIGURATION_ERROR)

    required_values = [
            'authorization_endpoint',
            'token_endpoint',
            'jwks_uri'
    ]

    missing_values = [value for value in required_values if value not in config]
    if missing_values:
        logger.exception(
            "The config received from the Identity Provider does not contain the required"
            "information. The following keys are missing in the config [%s]\n"
            "The server will be stopped now!" % ', '.join(missing_values)
        )
        exit(ErrorCodes.LOGIN_CONFIGURATION_ERROR)
    return config


def set_userinfo(
        response_object: Generic = None,
        access_token: str = None):

    logger.info("set_userinfo")
    j_user = requests.post(
        headers=dict(Authorization="Bearer " + access_token),
        url=current_app.OIDC_Login.oidc_config["userinfo_endpoint"]
    ).json()

    logger.info(j_user.get('preferred_username', None))
    response_object.set_cookie('name', j_user.get('preferred_username', None))


def requester_and_cookieSetter(
        payload: Dict[str, Any],
        response_object: Generic = None) -> Generic:

    # Make request
    try:
        j = requests.post(
            data=payload,
            url=current_app.OIDC_Login.oidc_config["token_endpoint"]
        ).json()

        # obtain access_token and refresh token from response
        at = j.get('access_token', None)
        ate = j.get('expires_in', None)

        # In case of an invalid session or user logout return error
        if 'error' in j:
            if response_object:
                return response_object
            j['refresh'] = False
            return j, 401

        set_access_cookies(response_object, at, ate)
        set_userinfo(response_object, at)

        csrf_token = j.get("session_state", str(uuid.uuid4()))
        response_object.set_cookie("X-CSRF Token", csrf_token)
        response_object.csrf_token = csrf_token

        logger.info("CSRF Token generated and set")
        return response_object

    except Exception as e:
        logger.exception("Login Process Failed")
        logger.exception(e)
        return (jsonify(
            {'refresh': False, 'msg': 'Login Process Failed'}
        ), 401)
