from flask import request, current_app


class User:
    """
    This class makes the content of the jwt easily accessible.
    """
    def __init__(self, jwt_data):
        tokenData = jwt_data
        self.id = tokenData.get(current_app.config["JWT_IDENTITY_CLAIM"], None)
        self.username = tokenData.get("name", "username")
        self.email_verified = tokenData.get("email_verified", None)
        self.preferred_username = request.cookies.get("name", "dummy")
        self.email = tokenData.get("email", None)
        # self.user_claims = tokenData.get('user_claims', None)
        self.auth_time = tokenData.get("auth_time", None)
        self.realm_roles = tokenData.get("realm_access", dict()).get('roles', [])
