import json
import logging
from time import sleep

import requests
from flask_jwt_extended import JWTManager
from jwt.algorithms import RSAAlgorithm

from dashboard.oidc.oidcUser import User
from dashboard.oidc.ErrorCodes import ErrorCodes
from dashboard.oidc.utils import validate_received_config_or_stop

logger = logging.getLogger(__name__)


class oidcLogin:
    """
    This Class Initializes the OIDC Login and creates a JWTManager
    during initialisation multiple additional endpoints will be created
    for an manual login.
    """

    def __init__(self, app):
        """

        app: Flask App object
        config: dictionary
        """

        # Check if the Login System can be Initialized
        if app.config["LOGIN"]:
            app.OIDC_Login = self

            # Request external configuration from Issuer URL
            config_url = "%s/.well-known/openid-configuration" % app.config["OIDC_ISSUER_URL"]
            for retry in range(4, -1, -1):
                try:
                    self.oidc_config = requests.get(config_url).json()
                    break
                except Exception as e:
                    logger.info(
                        'Unable to load endpoint path from\n"%s"\n'
                        'Probably wrong url in config file or maybe json error will retry %d times'
                        % (config_url, retry)
                    )
                    if not retry:
                        logger.exception("Timeout! Could not receive config from \
                                         the Identity Provider! Stopping Server!")
                        logger.exception(e)
                        exit(ErrorCodes.LOGIN_CONFIGURATION_ERROR)
                    sleep(20)

            # check existence of required information from Identity Provider or stop!
            validate_received_config_or_stop(self.oidc_config)
            logger.info("Loaded Issuer Config correctly!")

            # Use the obtained Issuer Config to obtain public key
            logger.info("Extracting Public Certificate from Issuer")
            try:
                self.oidc_jwks_uri_response = requests.get(self.oidc_config["jwks_uri"]).json()
            except Exception as e:
                logger.exception("Could not connect to %s to receive the RSA public key!"
                                 % self.oidc_config["jwks_uri"])
                logger.exception(e)
                exit(ErrorCodes.LOGIN_CONFIGURATION_ERROR)

            # retrieve first jwk entry from jwks_uri endpoint and use it to
            # construct the RSA public key
            try:
                app.config["JWT_PUBLIC_KEY"] = RSAAlgorithm.from_jwk(
                    json.dumps(self.oidc_jwks_uri_response["keys"][0]))
            except Exception as e:
                logger.exception("An exception occurred during RSA key extraction from %s "
                                 % self.oidc_jwks_uri_response)
                logger.exception(e)
                logger.exception("The server will be stopped now!")
                exit(ErrorCodes.LOGIN_CONFIGURATION_ERROR)

        # Here here the flask_jwt_extended module will be initialized
        self.jwt = JWTManager(app)

        @self.jwt.user_lookup_loader
        def user_loader_callback(_jwt_header, jwt_data) -> User:
            """
            This function returns a User Object if the flask_jwt_extended current_user is called
            :param identity: unused, since data of access token will be used here
            :return: User
            """
            logger.info("running user_loader_callback")
            return User(jwt_data)
