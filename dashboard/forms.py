from flask_wtf import FlaskForm
from wtforms import SubmitField, SelectField, TextAreaField, validators
from jsonschema import validate
from dashboard.workflows import get_workflow_names, get_workflow_config_schema
import json


def validate_config_field(form, field):
    try:
        config = json.loads(field.data)
        schema = get_workflow_config_schema(workflow_name=form.workflow.data)
        validate(instance=config, schema=schema)
    except Exception as e:
        print(e)
        raise validators.ValidationError("The config does not fit to the schema.")
    return None


class CreateRun(FlaskForm):
    workflow = SelectField("Workflow", choices=get_workflow_names(),
                           validators=[validators.DataRequired()])
    config = TextAreaField("Config", validators=[validators.DataRequired(), validate_config_field])
    submit = SubmitField("submit")
