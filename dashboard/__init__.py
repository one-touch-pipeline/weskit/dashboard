# flake8: noqa: C901
from flask import Flask
from logging.handlers import RotatingFileHandler
import os
import logging
from dashboard.oidc import Login

def create_app():
    app = Flask(__name__)
    app.config.from_prefixed_env(prefix = "WESKIT")

    #from dashboard.errors import bp as errors_bp
    #app.register_blueprint(errors_bp)

    from dashboard import routes
    app.register_blueprint(routes.bp)

    if not app.debug:
        if not os.path.exists('logs'):
            os.mkdir('logs')
        file_handler = RotatingFileHandler("/tmp/dashboard.log", maxBytes=10240,
                                        backupCount=10)
        file_handler.setFormatter(logging.Formatter(
            '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)

        app.logger.setLevel(logging.INFO)
        app.logger.info("WESkit Dashboard startup with url to WESkit: {}".format(app.config["API_URL"]))

    Login.oidcLogin(app)
    return app
