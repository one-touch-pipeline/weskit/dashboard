import requests
from flask import current_app, request
from typing import Optional


def prepareHeader() -> Optional[dict]:
    if current_app.config["LOGIN"]:
        cookie = request.cookies.get("access_token_cookie")
        if cookie:
            return dict(Authorization="Bearer " + cookie)
        else:
            return None
    else:
        return None


class Com():

    def GetServiceInfo(self):
        self.response = requests.get(
            "{}/ga4gh/wes/v1/service-info".format(current_app.config["API_URL"]),
            verify=current_app.config["VERIFY_HTTPS"])
        if self.response.status_code == 200:
            return self.response.json()
        else:
            return {"content": ""}

    def GetRun(self, RunID, headers=None):
        if headers is None:
            headers = prepareHeader()
        self.response = requests.get(
            "{}/ga4gh/wes/v1/runs/{}".format(current_app.config["API_URL"], RunID),
            headers=headers, verify=current_app.config["VERIFY_HTTPS"])
        if self.response.status_code == 200:
            return self.response.json()
        else:
            return {"content": ""}

    def getRunsExtended(self, headers=None):
        if headers is None:
            headers = prepareHeader()
        self.response = requests.get(
            "{}/ga4gh/wes/v1/runs".format(current_app.config["API_URL"]), headers=headers,
            verify=current_app.config["VERIFY_HTTPS"])
        if self.response.status_code == 200:
            return self.response.json()
        else:
            return {"content": "This WESkit deployment requires a user login to see the runs."}

    def postRun(self, workflow_data, headers=None):
        if headers is None:
            headers = prepareHeader()
        self.response = requests.post(
            "{}/ga4gh/wes/v1/runs".format(current_app.config["API_URL"]), data=workflow_data,
            headers=headers, verify=current_app.config["VERIFY_HTTPS"])
        if self.response.status_code == 200:
            return self.response.json()
        else:
            return {"content": f"Run submission was not successful: {self.response.json()}"}
