import json
import yaml
import os


workflow_info_path = os.environ.get("WESKIT_WORKFLOW_INFO_PATH",
                                    "tests/example_workflows/info.yaml")


with open(workflow_info_path, "r") as file:
    WORKFLOWS = yaml.safe_load(file)


def get_workflow_names():
    return WORKFLOWS.keys()


def get_workflow_config_schema(workflow_name):
    return WORKFLOWS[workflow_name]["config"]


def get_workflow_data(workflow_name, config_text):
    workflow_params = json.dumps(json.loads(config_text))
    workflow_data = {
        "workflow_params": workflow_params,
        "workflow_type": WORKFLOWS[workflow_name]["type"],
        "workflow_type_version": WORKFLOWS[workflow_name]["version"],
        "workflow_url": WORKFLOWS[workflow_name]["uri"]
    }
    return workflow_data
