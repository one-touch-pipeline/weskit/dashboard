import os


envvars_basic = [
    "WESKIT_DASHBOARD_URL",
    "WESKIT_API_URL",
    "WESKIT_LOGIN",
    "WESKIT_INFO__info_text",
    "WESKIT_INFO__imprint_name",
    "WESKIT_INFO__imprint_description",
    "WESKIT_INFO__imprint_address",
    "WESKIT_INFO__imprint_email"
]

envvars_login = [
    "JWT_COOKIE_SECURE",
    "JWT_TOKEN_LOCATION",
    "JWT_ALGORITHM",
    "JWT_DECODE_AUDIENCE",
    "JWT_IDENTITY_CLAIM",
    "JWT_CSRF_IN_COOKIES",
    "OIDC_ISSUER_URL",
    "OIDC_CLIENT_SECRET",
    "OIDC_REALM",
    "OIDC_CLIENTID",
    "OIDC_SCOPE"
]


def validate_envvars():
    try:
        for key in envvars_basic:
            os.environ[key]
        if os.environ["WESKIT_LOGIN"]:
            for key in envvars_login:
                os.environ[key]
    except KeyError as ke:
        print("Please define the environment variable:", ke)
    return True
