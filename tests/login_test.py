import requests
import pytest
import os
from flask import current_app


@pytest.fixture(name="runStorage", scope="class")
def runid_fixture():
    """
    This fixture returns a class that stores the run ID as variable within a Test Class.
    This makes it possible to exchange variables between tests of a class without using
    global variables accessible via "runStorage.runid" if runStorage is declared
    in the call of the testfunction.
    """

    class RunID:
        def __init__(self):
            self.runid = ""

    return RunID()


@pytest.fixture(name="OIDC_credentials", scope="session")
def login_fixture():
    """
    This fixture connects to a keycloak server requests an access token and
    returns a LoginClass object with the the following values:

    The access token which can be used as cookie.
    The header token which is the word "Bearer: " + access token
    The session token which is a specific value of the access token and is used for CSRF protection

    "OIDC_credentials" must be declared in the test function call to access this object.
    :return:
    OIDC_credentials.access_token
    OIDC_credentials.session_token
    OIDC_credentials.headerToken
    """

    class LoginClass:
        def __init__(self):
            payload = {
                "grant_type": "client_credentials",
                "client_id": "weskit",
                "client_secret": os.getenv("WESKIT_OIDC_CLIENT_SECRET")
            }

            r2 = requests.post(
                url=current_app.OIDC_Login.oidc_config["token_endpoint"],
                data=payload
            ).json()

            self.access_token = r2.get('access_token', "None2")
            self.session_token = {
                "X-Csrf-Token": r2.get('session_state', "None2")}
            self.headerToken = {
                'Authorization': 'Bearer %s' % self.access_token}

    return LoginClass()


class TestOpenEndpoint:
    """
    The TestOpenEndpoint class ensures that all endpoint that should be accessible
    without login are accessible.
    """

    def test_get_dashboard(self, login_test_client, weskit_mocker):
        response = login_test_client.get("/dashboard")
        assert response.status_code == 200


class TestWithoutLogin:
    """
    The TestWithoutLogin class ensures that all secured endpoints are not
    accessible without credentials.
    """

    def test_list_runs_wo_login(self, login_test_client, weskit_mocker):
        response = login_test_client.get("/runs")
        assert response.status_code == 401

    def test_get_run_wo_login(self, login_test_client, weskit_mocker):
        response = login_test_client.get(
            "/runs/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c")
        assert response.status_code == 401


class TestCSRFTokenOnly:
    """
    The TestCSRFTokenOnly class ensures that it is impossible to access the endpoint by only
    submitting the session token without an access token as Cookie.
    """

    def test_reject_get_runs_CSRF_only(
            self, login_test_client, OIDC_credentials, weskit_mocker):
        response = login_test_client.get(
            "/runs", headers=OIDC_credentials.session_token)
        assert response.status_code == 403

    def test_get_run_CSRF_only(self, login_test_client, OIDC_credentials, weskit_mocker):
        response = login_test_client.get(
            "/runs/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c",
            headers=OIDC_credentials.session_token)
        assert response.status_code == 403


class TestWithCookie:
    """
    This TestWithCookie tests the correct functionality of the
    secured endpoint by using cookies with session token
    """

    def test_accept_get_runs_cookie(self, login_test_client, OIDC_credentials, weskit_mocker):
        login_test_client.set_cookie(
            key='access_token_cookie',
            value=OIDC_credentials.access_token)
        response = login_test_client.get("/runs")
        # 302 is returned upon successful logout
        assert response.status_code in [200, 302]

    def test_accept_get_runlog_cookie(
            self, login_test_client, OIDC_credentials, weskit_mocker):
        login_test_client.set_cookie(
            key='access_token_cookie',
            value=OIDC_credentials.access_token)
        response = login_test_client.get(
            "/runs/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c")
        # 302 is returned upon successful logout
        assert response.status_code in [200, 302]
