import os
import pytest
import requests
import time
import logging
from testcontainers.core.container import DockerContainer
from tests.mock import Mock

logger = logging.getLogger(__name__)

logger = logging.getLogger(__name__)


def getContainerProperties(container, port):
    return (
        {
            "ExternalHostname": container.get_container_host_ip(),
            "InternalPorts": list(container.ports.keys()),
            "ExposedPorts": container.get_exposed_port(port),
            "InternalIP": container.get_docker_client().bridge_ip(container._container.id)
        }
    )


@pytest.fixture(scope="function")
def weskit_mocker(requests_mock):
    Mock(requests_mock)
    yield requests_mock


@pytest.fixture(scope="session")
def envars_nologin():
    os.environ["WESKIT_DASHBOARD_URL"] = "https://localhost:5001"
    os.environ["WESKIT_API_URL"] = "mock://localhost:5000"
    os.environ["WESKIT_LOGIN"] = "false"
    os.environ["WESKIT_INFO__info_text"] = "text"
    os.environ["WESKIT_INFO__imprint_name"] = "text"
    os.environ["WESKIT_INFO__imprint_description"] = "text"
    os.environ["WESKIT_INFO__imprint_address"] = "text"
    os.environ["WESKIT_INFO__imprint_email"] = "text"
    os.environ["WESKIT_VERIFY_HTTPS"] = "false"
    os.environ["WESKIT_SECRET_KEY"] = "xadfgfbvcbfghfdfddscvxvcx"
    os.environ["WESKIT_WORKFLOW_INFO_PATH"] = "tests/example_workflows/info.yaml"


@pytest.fixture(scope="session")
def nologin_test_client(envars_nologin):
    from dashboard import create_app
    app = create_app()
    app.testing = True
    # os.environ["testing_only_tokenendpoint"] = app.OIDC_Login.oidc_config["token_endpoint"]
    with app.test_client() as testing_client:
        with app.app_context():
            # This sets `current_app` for accessing the Flask app in the tests.
            yield testing_client


@pytest.fixture(scope="session")
def envars_login():
    os.environ["WESKIT_DASHBOARD_URL"] = "https://localhost:5001"
    os.environ["WESKIT_API_URL"] = "mock://localhost:5000"
    os.environ["WESKIT_LOGIN"] = "true"
    os.environ["WESKIT_INFO__info_text"] = "text"
    os.environ["WESKIT_INFO__imprint_name"] = "text"
    os.environ["WESKIT_INFO__imprint_address"] = "text"
    os.environ["WESKIT_INFO__imprint_email"] = "text"
    os.environ["WESKIT_JWT_COOKIE_SECURE"] = "true"
    os.environ["WESKIT_JWT_TOKEN_LOCATION"] = '["cookies", "headers"]'
    os.environ["WESKIT_JWT_ALGORITHM"] = "RS256"
    os.environ["WESKIT_JWT_DECODE_AUDIENCE"] = "account"
    os.environ["WESKIT_JWT_IDENTITY_CLAIM"] = "sub"
    os.environ["WESKIT_JWT_CSRF_IN_COOKIES"] = "false"
    os.environ["WESKIT_VERIFY_HTTPS"] = "false"
    os.environ["WESKIT_SECRET_KEY"] = "xadfgfbvcbfghfdfddscvxvcx"
    os.environ["WESKIT_OIDC_ISSUER_URL"] = "http://127.0.0.1:8080/realms/weskit"
    os.environ["WESKIT_OIDC_CLIENT_SECRET"] = "vzR9PtrYBLpOpJ110oz9trlZaJp5nNMP"
    os.environ["WESKIT_OIDC_REALM"] = "weskit"
    os.environ["WESKIT_OIDC_CLIENTID"] = "weskit"
    os.environ["WESKIT_OIDC_SCOPE"] = '["openid", "profile"]'
    os.environ["WESKIT_WORKFLOW_INFO_PATH"] = "tests/example_workflows/info.yaml"


@pytest.fixture(scope="session")
def login_test_client(keycloak_container, envars_login):
    keycloakContainerProperties = getContainerProperties(keycloak_container, '8080')
    # overwrite issuer url with container values
    os.environ["WESKIT_OIDC_ISSUER_URL"] = "http://%s:%s/realms/weskit" % (
        keycloakContainerProperties["ExternalHostname"],
        keycloakContainerProperties["ExposedPorts"],
    )
    from dashboard import create_app
    app = create_app()
    app.testing = True
    # os.environ["testing_only_tokenendpoint"] = app.OIDC_Login.oidc_config["token_endpoint"]
    with app.test_client() as testing_client:
        with app.app_context():
            # This sets `current_app` for accessing the Flask app in the tests.
            yield testing_client


@pytest.fixture(scope="session")
def keycloak_container():

    kc_container = DockerContainer("keycloak/keycloak:24.0.1")

    kc_container.with_command("start-dev --import-realm")
    kc_container.with_volume_mapping(host=os.path.abspath("tests/keycloak/realm-export.json"),
                                     container="/opt/keycloak/data/import/realm-export.json",
                                     mode="ro")

    hostname = "keycloak"
    kc_container.with_name(hostname)
    kc_container.with_exposed_ports('8080')

    kc_container.with_env("KEYCLOAK_ADMIN", "admin")
    kc_container.with_env("KEYCLOAK_ADMIN_PASSWORD", "admin")

    kc_container.with_env("KC_DB_USERNAME", "keycloak")
    kc_container.with_env("KC_DB_PASSWORD", "secret_password")

    with kc_container as keycloak:
        time.sleep(5)

        kc_port = keycloak.get_exposed_port('8080')
        kc_host = keycloak.get_container_host_ip()

        retry = 20
        waitingSeconds = 5
        kc_running = False
        for i in range(retry):
            try:
                requests.get("http://" + kc_host + ":" + kc_port)
                logger.warning("Retrying connecting to Keycloak container {}/{}".format(i, retry))
                kc_running = True

                break

            except Exception:
                logger.warning("Retrying connecting to Keycloak container {}/{}".format(i, retry))
                time.sleep(waitingSeconds)

        assert kc_running

        yield keycloak
