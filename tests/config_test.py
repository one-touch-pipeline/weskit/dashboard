from dashboard.config import validate_envvars


class TestConfig:
    """
    The TestWithoutLogin class ensures that all secured endpoints are not
    accessible without credentials.
    """

    def test_nologin(self, envars_nologin):
        assert validate_envvars()

    def test_login(self, envars_login):
        assert validate_envvars()
