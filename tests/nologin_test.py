import pytest


@pytest.fixture(name="runStorage", scope="class")
def runid_fixture():
    """
    This fixture returns a class that stores the run ID as variable within a Test Class.
    This makes it possible to exchange variables between tests of a class without using
    global variables accessible via "runStorage.runid" if runStorage is declared
    in the call of the testfunction.
    """

    class RunID:
        def __init__(self):
            self.runid = ""

    return RunID()


class TestOpenEndpoint:
    """
    The TestOpenEndpoint class ensures that all endpoint that should be accessible
    without login are accessible.
    """

    def test_get_root(self, nologin_test_client, weskit_mocker):
        response = nologin_test_client.get("/")
        assert response.status_code == 200

    def test_list_runs_wo_login(self, nologin_test_client, weskit_mocker):
        response = nologin_test_client.get("/runs")
        assert response.status_code == 200

    def test_get_run_wo_login(self, nologin_test_client, weskit_mocker):
        response = nologin_test_client.get(
            "/runs/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c")
        assert response.status_code == 200

    def test_post_run_wo_login(self, nologin_test_client, weskit_mocker):
        response1 = nologin_test_client.get("/submit")
        assert response1.status_code == 200

        data = {"config": '{"text": "hello world"}'}
        response2 = nologin_test_client.post("/submit", data=data)
        assert response2.status_code == 200
