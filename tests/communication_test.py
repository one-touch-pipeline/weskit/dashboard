from dashboard.com import Com


def test_ServiceInfo(login_test_client, weskit_mocker):
    mock_info = {
        "auth_instructions_url": "https://somewhere.org",
        "contact_info_url": "your@email.de",
        "default_workflow_engine_parameters": [
            {
                "default_value": "1",
                "name": "cores",
                "type": "int",
                "workflow_engine": "snakemake"}],
        "supported_filesystem_protocols": [
            "s3",
            "posix"],
        "supported_wes_versions": ["1.0.0"],
        "system_state_counts": {
            "CANCELED": 0,
            "CANCELING": 0,
            "COMPLETE": 2,
            "EXECUTOR_ERROR": 0,
            "INITIALIZING": 0,
            "PAUSED": 0,
            "QUEUED": 1,
            "RUNNING": 0,
            "SYSTEM_ERROR": 1,
            "UNKNOWN": 0},
        "tags": {
            "tag1": "value1",
            "tag2": "value2"},
        "workflow_engine_versions": {
            "Snakemake": "5.8.2"},
        "workflow_type_versions": {
            "Snakemake": {
                "workflow_type_version": ["5"]}}}
    commu = Com()
    service_info = commu.GetServiceInfo()
    for key in mock_info:
        assert key in service_info


def test_getRunsExtended(login_test_client, weskit_mocker):
    commu = Com()
    mock_runs = [{"request": {"workflow_params": {"text": "hello world"},
                              "workflow_type": "snakemake",
                              "workflow_type_version": "5.8.2",
                              "workflow_url": "file:tests/wf1/Snakefile"},
                  "run_id": "a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c",
                  "run_status": "COMPLETE",
                  "start_time": "2021-04-28T06:48:09Z",
                  "user_id": "6bd12400-6fc4-402c-9180-83bddbc30526"},
                 {"request": {"workflow_params": {"text": "hello world"},
                              "workflow_type": "snakemake",
                              "workflow_type_version": "5.8.2",
                              "workflow_url": "file:tests/wf1/Snakefile"},
                  "run_id": "92b2ed95-6a2a-4350-82ba-35a8825be736",
                  "run_status": "COMPLETE",
                  "start_time": "2021-04-28T06:48:10Z",
                  "user_id": "6bd12400-6fc4-402c-9180-83bddbc30526"}]
    res = commu.getRunsExtended(headers=dict())["runs"]
    for run in range(len(res)):
        for key in res[run]:
            assert key in mock_runs[run]


def test_GetRun(login_test_client, weskit_mocker):
    commu = Com()
    mock_run = {
        "outputs": {
            "Workflow": [
                "file:///data/a375/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c/command",
                "file:///data/a375/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c/config.yaml",
                "file:///data/a375/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c/hello_world.txt",
                "file:///data/a375/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c/stderr",
                "file:///data/a375/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c/stdout",
                "file:///data/a375/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c/.snakemake/log/2021-04-28T064810.061636.snakemake.log", # noqa
                "file:///data/a375/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c/.snakemake/metadata/aGVsbG9fd29ybGQudHh0"]}, # noqa
        "request": {
            "workflow_params": {
                "text": "hello world"},
            "workflow_type": "snakemake",
            "workflow_type_version": "5.8.2",
            "workflow_url": "file:tests/wf1/Snakefile"},
        "run_id": "a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c",
        "run_log": {
            "cmd": "workflow_type=snakemake, workflow_path=/workflows/tests/wf1/Snakefile, workdir=/data/a375/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c, config_files=['/data/a375/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c/config.yaml'], workflow_engine_params=[]"}, # noqa
        "state": "COMPLETE",
        "task_logs": [],
        "user_id": "6bd12400-6fc4-402c-9180-83bddbc30526"}
    result = commu.GetRun("a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c", headers=dict())
    not_found = "not_found"
    not_found_result = commu.GetRun(not_found, headers=dict())
    assert mock_run == result
    assert not_found_result == {"content": ""}


def test_PostRun(login_test_client, weskit_mocker):
    commu = Com()
    mock_post = {"run_id": "92b2ed95-6a2a-4350-82ba-35a8825be736"}
    result = commu.postRun(workflow_data={}, headers=dict())
    assert mock_post == result
