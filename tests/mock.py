# class for mocking the Weskit server
class Mock:

    def __init__(self, requests_mock):
        runs = [{"request": {"workflow_params": {"text": "hello world"},
                             "workflow_type": "snakemake",
                             "workflow_type_version": "5.8.2",
                             "workflow_url": "file:tests/wf1/Snakefile"},
                 "run_id": "a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c",
                 "run_status": "COMPLETE",
                 "start_time": "2021-04-28T06:48:09Z",
                 "user_id": "6bd12400-6fc4-402c-9180-83bddbc30526"},
                {"request": {"workflow_params": {"text": "hello world"},
                             "workflow_type": "snakemake",
                             "workflow_type_version": "5.8.2",
                             "workflow_url": "file:tests/wf1/Snakefile"},
                 "run_id": "92b2ed95-6a2a-4350-82ba-35a8825be736",
                 "run_status": "COMPLETE",
                 "start_time": "2021-04-28T06:48:10Z",
                 "user_id": "6bd12400-6fc4-402c-9180-83bddbc30526"}]
        service_info = {
            "auth_instructions_url": "https://somewhere.org",
            "contact_info_url": "your@email.de",
            "default_workflow_engine_parameters": [
                {
                    "default_value": "1",
                    "name": "cores",
                    "type": "int",
                    "workflow_engine": "snakemake"}],
            "supported_filesystem_protocols": [
                "s3",
                "posix"],
            "supported_wes_versions": ["1.0.0"],
            "system_state_counts": {
                "CANCELED": 0,
                "CANCELING": 0,
                "COMPLETE": 2,
                "EXECUTOR_ERROR": 0,
                "INITIALIZING": 0,
                "PAUSED": 0,
                "QUEUED": 1,
                "RUNNING": 0,
                "SYSTEM_ERROR": 1,
                "UNKNOWN": 0},
            "tags": {
                "tag1": "value1",
                        "tag2": "value2"},
            "workflow_engine_versions": {
                "Snakemake": "5.8.2"},
            "workflow_type_versions": {
                "Snakemake": {
                    "workflow_type_version": ["5"]}}}
        run1 = {
            "outputs": {
                "Workflow": [
                    "file:///data/a375/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c/command",
                    "file:///data/a375/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c/config.yaml",
                    "file:///data/a375/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c/hello_world.txt",
                    "file:///data/a375/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c/stderr",
                    "file:///data/a375/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c/stdout",
                    "file:///data/a375/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c/.snakemake/log/2021-04-28T064810.061636.snakemake.log", # noqa
                    "file:///data/a375/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c/.snakemake/metadata/aGVsbG9fd29ybGQudHh0"]}, # noqa
            "request": {
                "workflow_params": {
                    "text": "hello world"},
                "workflow_type": "snakemake",
                "workflow_type_version": "5.8.2",
                "workflow_url": "file:tests/wf1/Snakefile"},
            "run_id": "a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c",
            "run_log": {
                "cmd": "workflow_type=snakemake, workflow_path=/workflows/tests/wf1/Snakefile, "
                "workdir=/data/a375/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c, "
                "config_files=['/data/a375/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c/config.yaml'], "
                "workflow_engine_params=[]"},
            "state": "COMPLETE",
            "task_logs": [],
            "user_id": "6bd12400-6fc4-402c-9180-83bddbc30526"}
        run2 = {
            'outputs': {
                'Workflow': [
                    'file:///data/92b2/92b2ed95-6a2a-4350-82ba-35a8825be736/command',
                    'file:///data/92b2/92b2ed95-6a2a-4350-82ba-35a8825be736/config.yaml',
                    'file:///data/92b2/92b2ed95-6a2a-4350-82ba-35a8825be736/hello_world.txt',
                    'file:///data/92b2/92b2ed95-6a2a-4350-82ba-35a8825be736/stderr',
                    'file:///data/92b2/92b2ed95-6a2a-4350-82ba-35a8825be736/stdout',
                    'file:///data/92b2/92b2ed95-6a2a-4350-82ba-35a8825be736/.snakemake/log/2021-04-28T064810.618439.snakemake.log', # noqa
                    'file:///data/92b2/92b2ed95-6a2a-4350-82ba-35a8825be736/.snakemake/metadata/aGVsbG9fd29ybGQudHh0']}, # noqa
            'request': {
                'workflow_params': {
                    'text': 'hello world'},
                'workflow_type': 'snakemake',
                'workflow_type_version': '5.8.2',
                'workflow_url': 'file:tests/wf1/Snakefile'},
            'run_id': '92b2ed95-6a2a-4350-82ba-35a8825be736',
            'run_log': {
                'cmd': "workflow_type=snakemake, workflow_path=/workflows/tests/wf1/Snakefile, "
                       "workdir=/data/92b2/92b2ed95-6a2a-4350-82ba-35a8825be736, "
                       "config_files=['/data/92b2/92b2ed95-6a2a-4350-82ba-35a8825be736/config.yaml'], " # noqa
                       "workflow_engine_params=[]"},
            'state': 'COMPLETE',
            'task_logs': [],
            'user_id': '6bd12400-6fc4-402c-9180-83bddbc30526'}
        not_found = {}
        run1_stderr = {
            "content": "2021-04-28T06:48:09Z\nBuilding DAG of jobs...\nUsing shell: /bin/bash\n"
            "Provided cores: 1 (use --cores to define parallelism)\nRules claiming more threads "
            "will be scaled down.\nJob counts:\n\tcount\tjobs\n\t1\tall\n\t1\tstep1\n\t2\nSelect "
            "jobs to execute...\n\n[Wed Apr 28 06:48:10 2021]\nrule step1:\n    "
            "output: hello_world.txt\n    jobid: 1\n\n[Wed Apr 28 06:48:10 2021]\nFinished job 1."
            "\n1 of 2 steps (50%) done\nSelect jobs to execute...\n\n[Wed Apr 28 06:48:10 2021]\n"
            "localrule all:\n    input: hello_world.txt\n    jobid: 0\n\n[Wed Apr 28 06:48:10 "
            "2021]\nFinished job 0.\n2 of 2 steps (100%) done\nComplete log: /data/a375/"
            "a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c/.snakemake/log/2021-04-28T064810.061636."
            "snakemake.log\n"}
        run1_stdout = {"content": "2021-04-28T06:48:09Z\n"}
        post_run = {"run_id": "92b2ed95-6a2a-4350-82ba-35a8825be736"}

        requests_mock.get(
            "mock://localhost:5000/ga4gh/wes/v1/service-info",
            status_code=200,
            json=service_info)

        requests_mock.get(
            "mock://localhost:5000/ga4gh/wes/v1/runs",
            status_code=200,
            json={"runs": runs})

        requests_mock.get(
            "mock://localhost:5000/weskit/v1/runs",
            status_code=200,
            json={"runs": runs})

        requests_mock.get(
            'mock://localhost:5000/ga4gh/wes/v1/runs/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c',
            status_code=200,
            json=run1)

        requests_mock.get(
            'mock://localhost:5000/ga4gh/wes/v1/runs/92b2ed95-6a2a-4350-82ba-35a8825be736',
            status_code=200,
            json=run2)
        requests_mock.get(
            'mock://localhost:5000/weskit/v1/runs/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c/stderr',
            status_code=200,
            json=run1_stderr)
        requests_mock.get(
            'mock://localhost:5000/weskit/v1/runs/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c/stdout',
            status_code=200,
            json=run1_stdout)
        requests_mock.get(
            'mock://localhost:5000/ga4gh/wes/v1/runs/not_found',
            status_code=500,
            json=not_found)
        requests_mock.get(
            'mock://localhost:5000/ga4gh/wes/v1/runs/a3758d5a-ee70-4fd1-ac8d-fa2e5089b53c/json',
            status_code=200,
            json=run1)
        requests_mock.get(
            'mock://localhost:5000/ga4gh/wes/v1/runs/92b2ed95-6a2a-4350-82ba-35a8825be736/json',
            status_code=200,
            json=run2)
        requests_mock.post(
            "mock://localhost:5000/ga4gh/wes/v1/runs",
            status_code=200,
            json=post_run
        )
