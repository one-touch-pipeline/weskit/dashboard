from dashboard.workflows import get_workflow_data


TEST_WORKFLOW_DATA = {
    "workflow_params": '{"text": "hello world"}',
    "workflow_type": "SMK",
    "workflow_type_version": "7.30.2",
    "workflow_url": "file:wf1/Snakefile"
}


def test_get_workflow_data(envars_nologin):
    workflow_name = "wf1"
    config_text = '{"text": "hello world"}'
    workflow_data = get_workflow_data(workflow_name, config_text)
    assert workflow_data == TEST_WORKFLOW_DATA
