import dashboard.utils as u
from dashboard.com import Com
from pandas._testing import assert_frame_equal


def test_to_df(weskit_mocker):
    json_data = {
        "auth_instructions_url": "https://somewhere.org",
        "contact_info_url": "your@email.de",
        "default_workflow_engine_parameters": [
            {
                "default_value": "1",
                "name": "cores",
                "type": "int",
                "workflow_engine": "snakemake"}],
        "supported_filesystem_protocols": [
            "s3",
            "posix"],
        "supported_wes_versions": ["1.0.0"],
        "system_state_counts": {
            "CANCELED": 0,
            "CANCELING": 0,
            "COMPLETE": 2,
            "EXECUTOR_ERROR": 0,
            "INITIALIZING": 0,
            "PAUSED": 0,
            "QUEUED": 1,
            "RUNNING": 0,
            "SYSTEM_ERROR": 1,
            "UNKNOWN": 0},
        "tags": {
            "tag1": "value1",
            "tag2": "value2"},
        "workflow_engine_versions": {
            "Snakemake": "5.8.2"},
        "workflow_type_versions": {
            "Snakemake": {
                "workflow_type_version": ["5"]}}}

    commu = Com()
    service_info = commu.GetServiceInfo()
    df = u.to_df(service_info)
    df_mock = u.to_df(json_data)
    assert_frame_equal(df, df_mock)
