from dashboard.forms import validate_config_field
from dashboard.forms import CreateRun
from collections import namedtuple


def test_validate_config_field(envars_nologin):

    from dashboard import create_app
    app = create_app()
    app.testing = True
    with app.test_request_context():
        form = CreateRun()
        form.workflow.data = "wf1"
        field = namedtuple('field', ['data'])
        config_value = field('{"text": "hello world"}')
        validate_config_field(form, config_value)
