# WESkit Dashboard

FROM mambaorg/micromamba:1.5-lunar

EXPOSE 5001


# Capitalized versions for many tools. Minuscle version at least for apt.
ARG HTTP_PROXY=""
ARG http_proxy="$HTTP_PROXY"
ARG HTTPS_PROXY=""
ARG https_proxy="$HTTPS_PROXY"
ARG NO_PROXY=""
ARG no_proxy="$NO_PROXY"

# https://micromamba-docker.readthedocs.io/en/latest/advanced_usage.html#changing-the-user-id-or-name
# Changing the user id or name
ARG USER=weskit
ARG GROUP=weskit
ARG USER_ID="35671"
ARG USER_GID="35671"
USER root

RUN if grep -q '^ID=alpine$' /etc/os-release; then \
      # alpine does not have usermod/groupmod
      apk add --no-cache --virtual temp-packages shadow=4.13-r0; \
    fi && \
    usermod \
        "--login=$USER" \
        "--home=/home/weskit" \
        --move-home \
        "--uid=$USER_ID" \
        "$MAMBA_USER" && \
    groupmod \
        "--new-name=$GROUP" \
        "--gid=$USER_GID" \
        "$MAMBA_USER" && \
    if grep -q '^ID=alpine$' /etc/os-release; then \
      # remove the packages that were only needed for usermod/groupmod
      apk del temp-packages; \
    fi && \
    # Update the expected value of MAMBA_USER for the
    # _entrypoint.sh consistency check.
    echo "$USER" > "/etc/arg_mamba_user" && \
    :

RUN mkdir /tmp/dashboard \
    && chown -R "$USER:$GROUP" /tmp/dashboard /opt/conda \
    && chmod -R 0777 /home/weskit /tmp/dashboard /opt/conda \
    && umask 0770

RUN apt-get --allow-releaseinfo-change update && \
    apt-get install -y --no-install-recommends curl procps && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

USER $USER
ENV MAMBA_USER=$USER

RUN micromamba config set channel_priority strict
COPY environment.yaml /tmp/dashboard/environment.yaml
WORKDIR /tmp/dashboard

RUN echo "Creating environment ..." && \
    micromamba config set channel_priority flexible
RUN micromamba install -y -n base -f /tmp/dashboard/environment.yaml && \
    micromamba clean --all --yes
COPY ./ /tmp/dashboard

# If you would like an ENTRYPOINT command to be executed within an active conda environment, 
# then add "/usr/local/bin/_entrypoint.sh" as the first element of the JSON array argument to ENTRYPOINT. 

# -a "" for disabling stream redirection
ENTRYPOINT ["/usr/local/bin/_entrypoint.sh","micromamba", "run", "-a", "", "-n", "base"]
